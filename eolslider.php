<?php
/*
    Plugin Name: EOL SLider
    Description: A basic slider plugin that uses cycle2
    Author: Ben Dunkle (@empireoflight)
    Version: 1.0
*/

// add the shortcode
add_shortcode('eol_slider', 'eol_function');


// make sure the theme supports featured images

add_theme_support( 'post-thumbnails' );

// set the default size for the slide images

add_image_size('eol_slide', 1000, 400, true);

add_action('init', 'eol_init'); 

// create the custom post type
function eol_init() {
    $args = array(
        'public' => true,
        'label' => 'Slider Images',
        'supports' => array(
            'title',
            'thumbnail'
        ),
         'register_meta_box_cb' => 'add_custom_meta_box'
    );
    register_post_type('eol_images', $args);
}

// add a field for the slide order. adapted from http://www.sitepoint.com/adding-custom-meta-boxes-to-wordpress/
function add_custom_meta_box()
{
    add_meta_box("demo-meta-box", "Slide order", "custom_meta_box_markup", "eol_images", "side", "high", null);
}


function custom_meta_box_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
        <div>
            <label for="meta-box-text">Type in the slide order</label>
            <input name="meta-box-text" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text", true); ?>">
   </div>
    <?php  
}

function save_custom_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "eol_images";
    if($slug != $post->post_type)
        return $post_id;

    $meta_box_text_value = "";
    $meta_box_dropdown_value = "";
    $meta_box_checkbox_value = "";

    if(isset($_POST["meta-box-text"]))
    {
        $meta_box_text_value = $_POST["meta-box-text"];
    }   
    update_post_meta($post_id, "meta-box-text", $meta_box_text_value);

    
}

add_action("save_post", "save_custom_meta_box", 10, 3);

// add the slide order to the quickedit menu_order















//register the styles and scripts

function eol_register_scripts() {
    if (!is_admin()) {
        // register
        wp_register_script('eol_cycle_script', plugins_url('jquery.cycle2.min.js', __FILE__), array( 'jquery' ));
        wp_register_script('eol_script', plugins_url('script.js', __FILE__));
 
        // enqueue
        wp_enqueue_script('eol_cycle_script');
        wp_enqueue_script('eol_script');
    }
}
add_action('wp_print_scripts', 'eol_register_scripts');
 
function eol_register_styles() {
    // register
    wp_register_style('eol_styles', plugins_url('eol_slider.css', __FILE__));
    wp_register_style('eol_styles_theme', plugins_url('eol_default.css', __FILE__));
 
    // enqueue
    wp_enqueue_style('eol_styles');
    wp_enqueue_style('eol_styles_theme');
}

add_action('wp_print_styles', 'eol_register_styles');

// define the template for the slideshow

function eol_function($type='eol_function') {
    global $post;
    $options = get_option( 'eol_settings' );
    $args = array(
        'post_type' => 'eol_images',
        'posts_per_page' => -1,
          'meta_key' => 'meta-box-text',
        'order' => 'ASC',
        'orderby' => 'meta_value_num'
    );
    $result = '<div class="slidecontainer">';
    $result .= '<div class="slideshow eolSlider theme-default" ';
    $result .= 'data-cycle-fx="' . $options['eol_radio_field_0'] . '" ';
    $result .= 'data-cycle-caption="#alt-caption" data-cycle-caption-template="{{alt}}" ';
    $result .= 'data-cycle-pause-on-hover=true ';
    $result .= '>';

    
 
    //the loop
    $loop = new WP_Query($args);
    while ($loop->have_posts()) {
        $loop->the_post();
 
        $the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $type);
        $result .='<img title="'.get_the_title().'" src="' . $the_url[0] . '" data-thumb="' . $the_url[0] . '" alt="' . get_the_title() . '"/>';
    }
    $result .= '</div><!-- eolSlider -->';
    $result .= '<div id="alt-caption" class="center"></div>';
    $result .= '</div><!-- .slidecontainer -->';

    return $result;
}




// add the options page to the eol_images CPT menu.


add_action( 'admin_menu', 'eol_add_admin_menu' );
add_action( 'admin_init', 'eol_settings_init' );


function eol_add_admin_menu(  ) { 

    add_submenu_page( 'edit.php?post_type=eol_images', 'EOL Slider', 'EOL Slider', 'manage_options', 'eol_slider', 'eol_slider_options_page' );

}


function eol_settings_init(  ) { 

    register_setting( 'pluginPage', 'eol_settings' );

    add_settings_section(
        'eol_pluginPage_section', 
        'Slider settings', 
        'eol_settings_section_callback', 
        'pluginPage'
    );

    add_settings_field( 
        'eol_radio_field_0', 
        'Transition effect', 
        'eol_transition', 
        'pluginPage', 
        'eol_pluginPage_section' 
    );




}





function eol_transition() {
 
    $options = get_option( 'eol_settings' );
     
    $html = '<input type="radio" id="eol_radio_field_0_one" name="eol_settings[eol_radio_field_0]" value="fade"' . checked( 'fade', $options['eol_radio_field_0'], false ) . '/>';
    $html .= '<label for="eol_radio_field_0_one">Fade</label><br>';
     
    $html .= '<input type="radio" id="eol_radio_field_0_two" name="eol_settings[eol_radio_field_0]" value="fadeout"' . checked( 'fadeout', $options['eol_radio_field_0'], false ) . '/>';
    $html .= '<label for="eol_radio_field_0_two">Fadeout</label><br>';

     $html .= '<input type="radio" id="eol_radio_field_0_three" name="eol_settings[eol_radio_field_0]" value="scrollHorz"' . checked( 'scrollHorz', $options['eol_radio_field_0'], false ) . '/>';
    $html .= '<label for="eol_radio_field_0_three">Scroll Horizontally</label><br>';
     
    echo $html;
 
} // end sandbox_radio_element_callback





function eol_settings_section_callback(  ) { 

    echo __( 'Add your settings here.', 'wordpress' );

}


function eol_slider_options_page(  ) { 

    ?>
    <form action='options.php' method='post'>
        
        <h2>EOL Slider</h2>
        
        <?php
        settings_fields( 'pluginPage' );
        do_settings_sections( 'pluginPage' );
        submit_button();
        ?>
        
    </form>
    <?php

}



?>